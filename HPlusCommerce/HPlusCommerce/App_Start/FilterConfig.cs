﻿using HPlusCommerce.Filters;
using System.Web;
using System.Web.Mvc;

namespace HPlusCommerce
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CustomExceptionHandler());
            filters.Add(new LogRequestFilter());
        }
    }
}
