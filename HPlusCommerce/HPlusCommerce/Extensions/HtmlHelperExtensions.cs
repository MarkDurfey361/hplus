﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace System.Web.Mvc
{
    public static class HtmlHelperExtensions
    {

        public static IHtmlString Copyrigth(this HtmlHelper helper)
        {
            return helper.Raw($"&copy; H + Sport {DateTime.Now.Year}");
        }

        public static IHtmlString IconBars(this HtmlHelper helper, int count)
        {
            StringBuilder resultado = new StringBuilder();

            resultado.Append("");
            for (var i = 0; i < count; i++)
            {
                resultado = resultado.Append("<span class=" + "icon-bar" + "></span>");
            }

            return helper.Raw(resultado);
        }

    }
}