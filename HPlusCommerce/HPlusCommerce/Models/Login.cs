﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;

namespace HPlusCommerce.Models
{
    public class Login
    {
        [Required]
        [EmailAddress(ErrorMessage = "El usuario debe ser un email válido")]
        public string Username { get; set; }

        [Required]
        [MinLength(8, ErrorMessage = "El password es muy corto")]
        //[CommonPasswords(ModelBinderErrorMessageProvider = "El password es muy común")]
        public string Password { get; set; }

    }
}