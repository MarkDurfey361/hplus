﻿using HPlusCommerce.Models;
using System.Web.Mvc;
using System.Web.Security;

namespace HPlusCommerce.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Login request)
        {
            if (!ModelState.IsValid)
                return View(request);

            if(!string.IsNullOrEmpty(request.Username) && !string.IsNullOrEmpty(request.Password))
            {
                FormsAuthentication.SetAuthCookie(request.Username, false);
                return Redirect(FormsAuthentication.GetRedirectUrl(request.Username, false));
            }

            ViewBag.Failed = true;
            return View(request);
        }
    }
}